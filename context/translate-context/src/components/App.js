import React, {Component} from "react";
import UserCreate from "../components/userCreate";

//importing the context
import languageContext from "../context/languageContext";

class App extends Component {
  state = {language: "english"};
  onLanguageChange = (language) => {
    this.setState({language});
  };
  render() {
    return (
      <div className="ui container">
        <div>
          Select a Language :<br />
          <i
            className="flag us"
            onClick={() => this.onLanguageChange("english")}
          />
          <i
            className="flag nl"
            onClick={() => this.onLanguageChange("dutch")}
          />
        </div>
        {/* {this.state.language} */}
        <languageContext.Provider value={this.state.language}>
          <UserCreate />
        </languageContext.Provider>
      </div>
    );
  }
}

export default App;
