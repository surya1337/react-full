import React, {Component} from "react";

//importing the context
import languageContext from "../context/languageContext";

class Button extends Component {
  // static contextType = languageContext;//this will be used only when we are using this.context

  render() {
    // console.log(this.context);const text

    // const text = this.context === "english" ? "Submit" : "Voorleggen";
    return (
      <button className="ui button primary">
        <languageContext.Consumer>
          {(value) => (value === "english" ? "Submit" : "Voorleggen")}
        </languageContext.Consumer>
      </button>
    );
  }
}

export default Button;
