import React, {Component} from "react";
import {Field, reduxForm} from "redux-form";


class StreamForm extends Component {
  renderErrors({touched, error}) {
    if (touched && error) {
      return (
        <div className="ui error message">
          <div className="header">{error}</div>
        </div>
      );
    }
  }

  renderInput = (formProps) => {
    // console.log(formProps);
    return (
      <div className="field">
        <label>{formProps.label}</label>
        <input
          value={formProps.input.value}
          onChange={formProps.input.onChange}
        />
        {this.renderErrors(formProps.meta)}
      </div>
    );
  };

  onSubmit = (formValues) => {
    this.props.onSubmit(formValues);
  };

  render() {
    // console.log(this.props);
    return (
      <form
        className="ui form error"
        onSubmit={this.props.handleSubmit(this.onSubmit)}
      >
        <Field name="title" component={this.renderInput} label="Enter a Name" />
        <Field
          name="description"
          component={this.renderInput}
          label="Enter a Description"
        />
        <button className="ui button primary">Submit</button>
      </form>
    );
  }
}

const validate = (formValues) => {
  const errors = {};
  if (!formValues.title) {
    errors.title = "Title must not be EMPTY!";
  }
  if (!formValues.description) {
    errors.description = "Description must not be EMPTY!";
  }
  // console.log(formValues);
  return errors;
};

export default reduxForm({form: "streamForm", validate: validate})(StreamForm);
