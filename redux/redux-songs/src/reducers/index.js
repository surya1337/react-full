import {combineReducers} from "redux";

const songsReducer = () => {
  return [
    {title: "Black Pink : Play with Fire", duration: "05:23"},
    {title: "Black Pink : DuDuDu", duration: "04:22"},
    {title: "Chellamma", duration: "06:19"},
  ];
};

const selectedSongReducer = (selectedSong = null, action) => {
  if (action.type === "SONG_SELECTED") {
    return action.payload;
  } else {
    return selectedSong;
  }
};

export default combineReducers({
  songs: songsReducer,
  selectedSong: selectedSongReducer,
});
